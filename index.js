const config = require('node-config-env-value');
const axios = require('axios');

var argv = require('optimist').argv;

const amount = argv._[0];
const from = argv._[1];
const to = argv._[2];
axios
    .get(config.get('SERVER_URL') + '/api/converter/convert?amount=' + amount + '&from=' + from + '&to=' + to)
    .then(function (res) {
        console.log(res.data)
    })
    .catch(function (err) {
        console.log(err.response.data)
    });